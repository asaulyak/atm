angular.module('atm')
	.factory('CardService', ['$http', CardService]);

function CardService($http) {
	return {
		cardNumber: '1459641278457796',

		pinCode: '',

		balance: '1254788',

		getCardInformation: function (cardNumber) {
			return $http.get('http://localhost:3080/api/cards/' + cardNumber);
		}
	};
}