(function () {
	'use strict';

	function CreditCardController($scope, $location, CardService, Keyboard) {

		$scope.card = CardService;
		$scope.Keyboard = Keyboard;

		$scope.$watch('Keyboard.pressedButton', function (button) {
			if(button) {
				switch (button.role) {
					case 'num':
						if($scope.card.cardNumber.cardNumber < 16) {
							$scope.card.cardNumber += button.value;
						}
						break;
					case 'clear':
						$scope.card.cardNumber = '';
						break;
					case 'done':
						CardService.getCardInformation($scope.card.cardNumber)
							.then(function (card) {
								if (!card.blocked) {
									$location.path('/pin');
								}
								else {
									$location.path('/blocked');
								}
							})
							.catch(function (error) {
								$location.path('/pin');
							});
						break;
				}
			}
		});
	}

	angular
		.module('atm')
		.controller('CreditCardController', ['$scope', '$location', 'CardService', 'Keyboard',
			CreditCardController]);
})();
