(function () {
	'use strict';

	function PinController ($scope, $location, $routeParams, CardService, Keyboard) {
		$scope.card = CardService;
		$scope.Keyboard = Keyboard;

		$scope.card.cardNumber = $routeParams.cardNumber;

		$scope.$watch('Keyboard.pressedButton', function (button) {
			if(button) {
				switch (button.role) {
					case 'num':
						if($scope.card.pinCode.length < 4) {
							$scope.card.pinCode += button.value;
						}
						break;
					case 'clear':
						$scope.card.pinCode = '';
						break;
					case 'done':
						CardService.getCardInformation($scope.card.pinCode)
							.then(function (card) {
								if (!card.blocked) {
									$location.path('/pin');
								}
								else {
									$location.path('/blocked');
								}
							})
							.catch(function (error) {
								$location.path('/pin');
							});
						break;
				}
			}
		});
	}

	angular
		.module('atm')
		.controller('PinController', ['$scope', '$location', '$routeParams', 'CardService', 'Keyboard', PinController]);
})();
