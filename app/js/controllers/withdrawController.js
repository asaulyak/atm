(function () {
	'use strict';

	function WithdrawController ($scope, $routeParams, CardService, Keyboard) {
		$scope.card = CardService;
		$scope.Keyboard = Keyboard;
		$scope.session = $routeParams.session;
		$scope.sum = '0';

		$scope.$watch('Keyboard.pressedButton', function (button) {
			if(button) {
				switch (button.role) {
					case 'num':
							$scope.sum += button.value;
						break;
					case 'clear':
						$scope.sum = '0';
						break;
					case 'done':
						//CardService.getCardInformation($scope.card.cardNumber)
						//	.then(function (card) {
						//		if (!card.blocked) {
						//			$location.path('/pin');
						//		}
						//		else {
						//			$location.path('/blocked');
						//		}
						//	})
						//	.catch(function (error) {
						//		$location.path('/pin');
						//	});
						break;
				}
			}
		});
	}

	angular
		.module('atm')
		.controller('WithdrawController', ['$scope', '$routeParams', 'CardService', 'Keyboard', WithdrawController]);
})();



