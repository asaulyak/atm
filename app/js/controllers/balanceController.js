(function () {
	'use strict';

	function BalanceController ($scope, $routeParams, CardService) {
		$scope.card = CardService;

		$scope.date = Date.now();

		$scope.session = $routeParams.session;
	}

	angular
		.module('atm')
		.controller('BalanceController', ['$scope', '$routeParams', 'CardService', BalanceController]);
})();


