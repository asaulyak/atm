'use strict';

// Declare app level module which depends on views, and components
angular.module('atm', [
	'ngRoute'
]).config(['$routeProvider', function ($routeProvider) {
	$routeProvider
		.when('/', {
			controller: 'CreditCardController',
			templateUrl: 'views/home.html'
		})
		.when('/pin/:cardNumber', {
			controller: 'PinController',
			templateUrl: 'views/pin.html'
		})
		.when('/operations/:session', {
			controller: 'OperationsController',
			templateUrl: 'views/operations.html'
		})
		.when('/balance/:session', {
			controller: 'BalanceController',
			templateUrl: 'views/balance.html'
		})
		.when('/withdraw/:session', {
			controller: 'WithdrawController',
			templateUrl: 'views/withdraw.html'
		})
		.otherwise({
			redirectTo: '/view1'
		});
}]);
